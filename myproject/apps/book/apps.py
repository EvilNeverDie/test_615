from django.apps import AppConfig

from apps import book


class BookConfig(AppConfig):
    name = 'book'
    verbose_name = 'книги'
