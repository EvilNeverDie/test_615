import datetime
from django.db import models
from django.utils import timezone


class Book(models.Model):
    book_title = models.CharField('название книги', max_length=200)
    book_author = models.CharField('автор книги', max_length=200)
    book_description = models.TextField('Описание книги')
    pub_date = models.DateField('дата публикации')

    def __str__(self):
        return self.book_title

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'


