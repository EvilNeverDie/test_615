from django.http import Http404, HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render
from .models import Book
from django.urls import reverse


def index(request):
    latest_book = Book.objects.order_by('book_title')
    return render(request, 'book/list.html', {'latest_book': latest_book})


def detail(request, article_id):
    try:
        a = Book.objects.get(id=article_id)
    except:
        raise Http404("Книга не найдена")
    return render(request, 'book/detail.html', {'book': a})


def add(request):
    return render(request, 'book/add.html')


def add_book(request):
    print(request.POST)
    a = Book(book_title=request.POST['title'], book_author=request.POST['author'],
             book_description=request.POST['description'],
             pub_date=request.POST['date']
             )
    a.save()
    return HttpResponseRedirect(reverse('book:index'))
