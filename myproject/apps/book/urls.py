from django.urls import path

from . import views

app_name = 'book'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:article_id>/', views.detail, name='detail'),
    path('add', views.add, name='add'),
    path('add_book', views.add_book, name='add_book')
]